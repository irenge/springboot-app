package com.vtech.pch.pchmgmtapp.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vtech.pch.pchmgmtapp.entity.Order;
import com.vtech.pch.pchmgmtapp.entity.OrderDetail;
import com.vtech.pch.pchmgmtapp.entity.Product;
import com.vtech.pch.pchmgmtapp.model.CartInfo;
import com.vtech.pch.pchmgmtapp.model.CartLineInfo;
import com.vtech.pch.pchmgmtapp.model.CustomerInfo;
import com.vtech.pch.pchmgmtapp.model.OrderDetailInfo;
import com.vtech.pch.pchmgmtapp.model.OrderInfo;
import com.vtech.pch.pchmgmtapp.pagination.PaginationResult;

@Transactional
@Repository
public class OrderDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private ProductDAO productDAO;

    private int getMaxOrderNum() {
        String sql = "Select max(o.orderNum) from " + Order.class.getName() + " o ";
        Session session = this.sessionFactory.getCurrentSession();
        Query < Integer > query = session.createQuery(sql, Integer.class);
        Integer value = (Integer) query.getSingleResult();
        if (value == null) {
            return 0;
        }
        return value;
    }



    @Transactional(rollbackFor = Exception.class)
    public void saveOrder(CartInfo cartInfo) {
        Session session = this.sessionFactory.getCurrentSession();

        int orderNum = this.getMaxOrderNum() + 1;
        Order order = new Order();

        order.setId(UUID.randomUUID().toString());
        order.setOrderNum(orderNum);
        order.setOrderDate(new Date());
        order.setAmount(cartInfo.getAmountTotal());

        CustomerInfo customerInfo = cartInfo.getCustomerInfo();
        order.setCustomerName(customerInfo.getName());
        order.setCustomerEmail(customerInfo.getEmail());
        order.setCustomerPhone(customerInfo.getPhone());
        order.setCustomerAddress(customerInfo.getAddress());

        session.persist(order);

        List < CartLineInfo > lines = cartInfo.getCartLines();

        for (CartLineInfo line: lines) {
            OrderDetail detail = new OrderDetail();
            detail.setId(UUID.randomUUID().toString());
            detail.setOrder(order);
            detail.setAmount(line.getAmount());
            detail.setPrice(line.getProductInfo().getPrice());
            detail.setQuanity(line.getQuantity());

            String code = line.getProductInfo().getCode();
            Product product = this.productDAO.findProduct(code);
            detail.setProduct(product);

            session.persist(detail);
        }

        // Order Number!
        cartInfo.setOrderNum(orderNum);
        // Flush
        session.flush();
    }

    // @page = 1, 2, ...
    public PaginationResult < OrderInfo > listOrderInfo(int page, int maxResult, int maxNavigationPage) {
        String sql = "Select new " + OrderInfo.class.getName() //
            +
            "(ord.id, ord.orderDate, ord.orderNum, ord.amount, " +
            " ord.customerName, ord.customerAddress, ord.customerEmail, ord.customerPhone) " + " from " +
            Order.class.getName() + " ord " //
            +
            " order by ord.orderNum desc";

        System.out.println("order query sql" + sql);
        Session session = this.sessionFactory.getCurrentSession();
        Query < OrderInfo > query = session.createQuery(sql, OrderInfo.class);
        return new PaginationResult < OrderInfo > (query, page, maxResult, maxNavigationPage);
    }

    public Order findOrder(String orderId) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.find(Order.class, orderId);
    }


    public OrderDetail findOrderDetail(String id) {
        try {
            String sql = "Select e from " + OrderDetail.class.getName() + " e Where e.id =:id ";

            Session session = this.sessionFactory.getCurrentSession();
            Query < OrderDetail > query = session.createQuery(sql, OrderDetail.class);
            query.setParameter("id", id);
            return (OrderDetail) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


    public OrderInfo getOrderInfo(String orderId) {
        Order order = this.findOrder(orderId);
        if (order == null) {
            return null;
        }
        return new OrderInfo(order.getId(), order.getOrderDate(), //
            order.getOrderNum(), order.getAmount(), order.getCustomerName(), //
            order.getCustomerAddress(), order.getCustomerEmail(), order.getCustomerPhone());
    }

    public List < OrderDetailInfo > listOrderDetailInfos(String orderId) {
        String sql = "Select new " + OrderDetailInfo.class.getName() //
            +
            "(d.id, d.product.code, d.product.name , d.quanity,d.price,d.amount) " //
            +
            " from " + OrderDetail.class.getName() + " d " //
            +
            " where d.order.id = :orderId ";

        Session session = this.sessionFactory.getCurrentSession();
        Query < OrderDetailInfo > query = session.createQuery(sql, OrderDetailInfo.class);
        query.setParameter("orderId", orderId);

        return query.getResultList();
    }


    // Delete Order
    public void deleteOrder(String orderId) {
        String sql = "Delete new " + OrderInfo.class.getName() //
            +
            "Where (ord.id = " + orderId + ")" + " from " +
            Order.class.getName() + " ord ";
        System.out.println("query delete order : " + sql);
        Session session = this.sessionFactory.getCurrentSession();
        Order order = session.find(Order.class, orderId);
        session.delete(order);
    }

    // Edit Order
    @Transactional(rollbackFor = Exception.class)
    public void saveCustommerOrder(OrderInfo orderInfo, String orderId) {
        Session session = this.sessionFactory.getCurrentSession();

        Order order = findOrder(orderId);

        order.setCustomerName(orderInfo.getCustomerName());
        order.setCustomerEmail(orderInfo.getCustomerEmail());
        order.setCustomerPhone(orderInfo.getCustomerPhone());
        order.setCustomerAddress(orderInfo.getCustomerAddress());
        order.setOrderDate(new Date());

        session.persist(order);

        // Flush
        session.flush();
    }

    // Edit Order
    @Transactional(rollbackFor = Exception.class)
    public void updateQuantityAndSave(OrderInfo orderInfo, String orderId, List < String > idOrderInfo) {
        Session session = this.sessionFactory.getCurrentSession();
        Order order = findOrder(orderId);
        OrderDetail orderDetail = new OrderDetail();

        double amountItem = 0;
        double amountTotal = 0;
        int qte = 0;
        double price = 0;

        List < OrderDetailInfo > details = orderInfo.getDetails();
        List < OrderDetailInfo > orderDetailInfoList = new ArrayList < > ();
        List < String > idOrderInfoList = idOrderInfo;
        if (details != null) {
            for (int i=0; i<=details.size()-1; i++) {
            	
            	OrderDetailInfo orderDetailInfo = details.get(i);
                qte = orderDetailInfo.getQuanity();
                price = orderDetailInfo.getPrice();
                amountItem = (double) price * qte;
                amountTotal += amountItem;

                orderDetailInfo.setQuanity(qte);
                orderDetailInfo.setPrice(price);
                orderDetailInfo.setAmount(amountItem);
                               

                if(idOrderInfoList != null) {
	                for (int index = 0 ; index <= idOrderInfoList.size()-1; index ++) {
	
	                	String id = idOrderInfoList.get(index);
	                    orderDetail = findOrderDetail(id);
	
	                    if (orderDetail != null && index == i) {
	                        orderDetail.setId(id);
	                        orderDetail.setOrder(order);
	                        orderDetail.setQuanity(qte);
	                        orderDetail.setPrice(price);
	                        orderDetail.setAmount(amountItem);
	                        Product product = this.productDAO.findProduct(orderDetailInfo.getProductCode());
	                        orderDetail.setProduct(product);
	                        
	                        orderDetail.setOrder(order);
	    	                session.persist(orderDetail);
	    	                
	    	                orderDetailInfoList.add(orderDetailInfo);
	                    }
	                    
	                }
                }
               
            }

        }
        
        // Mise a jour des info de la commandes

        orderInfo.setCustomerName(order.getCustomerName());
        orderInfo.setCustomerEmail(order.getCustomerEmail());
        orderInfo.setCustomerPhone(order.getCustomerPhone());
        orderInfo.setCustomerAddress(order.getCustomerAddress());
        orderInfo.setAmount(amountTotal);
        orderInfo.setOrderDate(new Date());
        orderInfo.setDetails((List < OrderDetailInfo > ) orderDetailInfoList);
        
        if (qte >= 0) {
            order.setAmount(amountTotal);           
        }
              
        session.persist(order);

        // Flush
        session.flush();
    }

}