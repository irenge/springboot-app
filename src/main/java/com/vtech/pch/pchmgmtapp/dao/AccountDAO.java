package com.vtech.pch.pchmgmtapp.dao;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.vtech.pch.pchmgmtapp.entity.Account;
import com.vtech.pch.pchmgmtapp.form.AccountForm;
import com.vtech.pch.pchmgmtapp.model.AccountInfo;
import com.vtech.pch.pchmgmtapp.pagination.PaginationResult;
 
@Transactional
@Repository
public class AccountDAO {
 
    @Autowired
    private SessionFactory sessionFactory;
 
    public Account findAccount(String userName) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.find(Account.class, userName);
    }
    
    
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void save(AccountForm accountForm) {
 
        Session session = this.sessionFactory.getCurrentSession();
        String username = accountForm.getUsername();
 
        Account account = null;
 
        boolean isNew = false;
        if (username != null) {
        	account = this.findAccount(username);
        }
        if (account == null) {
            isNew = true;
            account = new Account();
            account.setCreateAt(new Date());           
        }
        
        account.setUserName(username);
        // Password encoded
        if(accountForm.getPassword() != null || StringUtils.isNotEmpty(accountForm.getPassword())){
        	String pwdToEncode = accountForm.getPassword();
        	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    		String hashedPassword = passwordEncoder.encode(pwdToEncode);
    		account.setEncrytedPassword(hashedPassword);
    		System.out.println("password to encode "+pwdToEncode+" Hashed pwd :"+hashedPassword);
        }
        
        account.setActive(accountForm.isActive());
        account.setUserRole(accountForm.getUserRole());
 
        if (account.getImage() != null) {
            byte[] image = null;
            try {
                image = accountForm.getFileData().getBytes();
            } catch (IOException e) {
            }
            if (image != null && image.length > 0) {
                account.setImage(image);
            }
        }
        if (isNew) {
            session.persist(account);
        }
        // If error in DB, Exceptions will be thrown out immediately
        session.flush();
    }
    
    
    // @page = 1, 2, ...
    public PaginationResult<AccountInfo> listAccountInfo(int page, int maxResult, int maxNavigationPage) {
        String sql = "Select new " + AccountInfo.class.getName()//
                + "(act.userName, act.active, act.encrytedPassword, act.userRole, "
                + " act.createAt, act.loggedDate) " + " from "
                + Account.class.getName() + " act "//
                + " order by act.createAt desc";
         System.out.println("query account sql : "+sql);
        Session session = this.sessionFactory.getCurrentSession();
        Query<AccountInfo> query = session.createQuery(sql, AccountInfo.class);
        return new PaginationResult<AccountInfo>(query, page, maxResult, maxNavigationPage);
    }
    
    // Delete Account
    public void deleteAccount(String userName) {
    	String sql = "Delete new " + AccountInfo.class.getName()//
                 + "Where (act.userName = "+userName+")" + " from "
                 + Account.class.getName() + " act ";
    	System.out.println("query delete account : "+sql);
        Session session = this.sessionFactory.getCurrentSession();
        Account account = session.find(Account.class, userName);       
        session.delete(account);
    }
    
    // Set logged date
    public void setLoggedDateToUser (Date date, String userName) {
    	String sql = "Update new " + AccountInfo.class.getName()//
    			+ " Set (act.logged_date = "+date+")"
                + "Where (act.userName = "+userName+")" + " from "
                + Account.class.getName() + " act ";
	   	System.out.println("query update account : "+sql);
	   	Session session = this.sessionFactory.getCurrentSession();
	    Account account = session.find(Account.class, userName); 
	    
	    if(account != null) {
	    	 account.setLoggedDate(new Date());
	    	 session.update(account);
	    }	   
    }
    
 
}