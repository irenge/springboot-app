package com.vtech.pch.pchmgmtapp.dao;

import java.util.UUID;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.vtech.pch.pchmgmtapp.entity.Reclamation;
import com.vtech.pch.pchmgmtapp.form.ReclamationForm;


@Transactional
@Repository
public class CustomerDAO {
	
	@Autowired
    private SessionFactory sessionFactory;
	
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void save(ReclamationForm reclamationForm) {
		
		Session session = this.sessionFactory.getCurrentSession();
		
		Reclamation reclamation= new Reclamation();
		
		reclamation.setId(UUID.randomUUID().toString());
		
		if(reclamationForm.getName() != null) {
		    reclamation.setName(reclamationForm.getName());
		}
		
		if(reclamationForm.getEmail() != null) {
		    reclamation.setEmail(reclamationForm.getEmail());
		}
		
		if(reclamationForm.getSubject() != null) {
		    reclamation.setSubject(reclamationForm.getSubject());
		}
		
		if(reclamationForm.getMessage() != null) {
		    reclamation.setMessage(reclamationForm.getMessage());
		}
		
		if(reclamation != null) {
			session.persist(reclamation);
		}
		
		session.flush();
	}
}

