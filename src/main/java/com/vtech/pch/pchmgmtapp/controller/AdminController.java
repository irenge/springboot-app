package com.vtech.pch.pchmgmtapp.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.vtech.pch.pchmgmtapp.dao.AccountDAO;
import com.vtech.pch.pchmgmtapp.dao.OrderDAO;
import com.vtech.pch.pchmgmtapp.dao.ProductDAO;
import com.vtech.pch.pchmgmtapp.entity.Account;
import com.vtech.pch.pchmgmtapp.entity.Order;
import com.vtech.pch.pchmgmtapp.entity.Product;
import com.vtech.pch.pchmgmtapp.form.AccountForm;
import com.vtech.pch.pchmgmtapp.form.ProductForm;
import com.vtech.pch.pchmgmtapp.model.AccountInfo;
import com.vtech.pch.pchmgmtapp.model.OrderDetailInfo;
import com.vtech.pch.pchmgmtapp.model.OrderInfo;
import com.vtech.pch.pchmgmtapp.pagination.PaginationResult;
import com.vtech.pch.pchmgmtapp.validator.ProductFormValidator;
 
@Controller
@Transactional
public class AdminController {
 
   @Autowired
   private OrderDAO orderDAO;
   
   @Autowired
   private AccountDAO accountDAO;
 
   @Autowired
   private ProductDAO productDAO;
 
   @Autowired
   private ProductFormValidator productFormValidator;
 
   @InitBinder
   public void myInitBinder(WebDataBinder dataBinder) {
      Object target = dataBinder.getTarget();
      if (target == null) {
         return;
      }
      System.out.println("Target=" + target);
 
      if (target.getClass() == ProductForm.class) {
         dataBinder.setValidator(productFormValidator);
      }
   }
 
   // GET: Show Login Page
   @RequestMapping(value = { "/admin/login" }, method = RequestMethod.GET)
   public String login(Model model) {	  
      return "login";
   }
 
   @RequestMapping(value = { "/admin/accountInfo" }, method = RequestMethod.GET)
   public String accountInfo(Model model, @ModelAttribute("accountInfo") Account accountInfo) {
 
      UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
      if(userDetails.getUsername() != null) {
	      this.accountDAO.setLoggedDateToUser(new Date(), userDetails.getUsername());
	      System.out.println(userDetails.getPassword());
	      System.out.println(userDetails.getUsername());
	      System.out.println(userDetails.isEnabled());
      }
        
      accountInfo = this.accountDAO.findAccount(userDetails.getUsername());
      
      
      model.addAttribute("message-success", "Sucess your are logged !!!");
      model.addAttribute("userDetails", userDetails);
      model.addAttribute("accountInfo",accountInfo);
      return "accountInfo";
   }
   
 
   @RequestMapping(value = { "/admin/orderList" }, method = RequestMethod.GET)
   public String orderList(Model model, //
         @RequestParam(value = "page", defaultValue = "1") String pageStr) {
      int page = 1;
      try {
         page = Integer.parseInt(pageStr);
      } catch (Exception e) {
      }
      final int MAX_RESULT = 5;
      final int MAX_NAVIGATION_PAGE = 10;
 
      PaginationResult<OrderInfo> paginationResult //
            = orderDAO.listOrderInfo(page, MAX_RESULT, MAX_NAVIGATION_PAGE);
 
      model.addAttribute("paginationResult", paginationResult);
      return "orderList";
   }
 
   
   @RequestMapping(value = { "/admin/accountList" }, method = RequestMethod.GET)
   public String accountList(Model model, //
         @RequestParam(value = "page", defaultValue = "1") String pageStr) {
      int page = 1;
      try {
         page = Integer.parseInt(pageStr);
      } catch (Exception e) {
      }
      final int MAX_RESULT = 5;
      final int MAX_NAVIGATION_PAGE = 10;
 
      PaginationResult<AccountInfo> paginationResult //
            = accountDAO.listAccountInfo(page, MAX_RESULT, MAX_NAVIGATION_PAGE);
 
      model.addAttribute("paginationResult", paginationResult);
      return "accountList";
   }
   
   // GET: Show product.
   @RequestMapping(value = { "/admin/product" }, method = RequestMethod.GET)
   public String product(Model model, @RequestParam(value = "code", defaultValue = "") String code) {
      ProductForm productForm = null;
 
      if (code != null && code.length() > 0) {
         Product product = productDAO.findProduct(code);
         if (product != null) {
            productForm = new ProductForm(product);
         }
      }
      if (productForm == null) {
         productForm = new ProductForm();
         productForm.setNewProduct(true);
      }
      model.addAttribute("productForm", productForm);
      return "product";
   }
       
   
   // POST: Save product
   @RequestMapping(value = { "/admin/product" }, method = RequestMethod.POST)
   public String productSave(Model model, //
         @ModelAttribute("productForm") @Validated ProductForm productForm, //
         BindingResult result, //
         final RedirectAttributes redirectAttributes) {
 
      if (result.hasErrors()) {
         return "product";
      }
      try {
         productDAO.save(productForm);
      } catch (Exception e) {
         Throwable rootCause = ExceptionUtils.getRootCause(e);
         String message = rootCause.getMessage();
         model.addAttribute("errorMessage", message);
         // Show product form.
         return "product";
      }
 
      return "redirect:/productList";
   }
 
   // GET: Show account
   @RequestMapping(value = { "/admin/account" }, method = RequestMethod.GET)
   public String account(Model model, @RequestParam(value = "username", defaultValue = "") String username) {
      AccountForm accountForm = null;
 
      if (username != null && username.length() > 0) {
         Account account = accountDAO.findAccount(username);
         if (account != null) {
             accountForm = new AccountForm(account);
         }
      }
      if (accountForm == null) {
    	  accountForm = new AccountForm();
    	  accountForm.setNewAccount(true);
      }
      model.addAttribute("username",username);
      model.addAttribute("accountForm", accountForm);
      return "account";
   }
   
   
 
   // POST: Save account
   @RequestMapping(value = { "/admin/account" }, method = RequestMethod.POST)
   public String accountSave(Model model, //
         @ModelAttribute("accountForm") @Validated AccountForm accountForm, //
         BindingResult result, //
         final RedirectAttributes redirectAttributes) {
 
      if (result.hasErrors()) {
         return "account";
      }
      try {
         accountDAO.save(accountForm);
      } catch (Exception e) {
         Throwable rootCause = ExceptionUtils.getRootCause(e);
         String message = rootCause.getMessage();
         model.addAttribute("errorMessage", message);
         // Show product form.
         return "account";
      }
 
      return "redirect:/admin/accountInfo";
   }
   
   // order details
   @RequestMapping(value = { "/admin/order" }, method = RequestMethod.GET)
   public String orderView(Model model, @RequestParam("orderId") String orderId) {
      OrderInfo orderInfo = null;
      if (orderId != null) {
         orderInfo = this.orderDAO.getOrderInfo(orderId);
      }
      if (orderInfo == null) {
         return "redirect:/admin/orderList";
      }
      List<OrderDetailInfo> details = this.orderDAO.listOrderDetailInfos(orderId);
      orderInfo.setDetails(details);
 
      model.addAttribute("orderInfo", orderInfo);
 
      return "order";
   }
   
  // orders detail edit 
   @RequestMapping(value = { "/admin/orderDetailEdit" }, method = RequestMethod.GET)
   public String orderView(HttpServletRequest request, HttpSession session ,  
		   Model model, @RequestParam("orderId") String orderId, @RequestParam(value = "page", defaultValue = "1") String pageStr) {
      OrderInfo orderInfo = null;
      
      if (orderId != null) {
    	 session.setAttribute("orderId", orderId);
         orderInfo = this.orderDAO.getOrderInfo(orderId);
      }
      

      if (orderInfo == null) {
         return "redirect:/admin/orderDetailEdit";
      }

	  int page = 1;
      try {
         page = Integer.parseInt(pageStr);
      } catch (Exception e) {
      }
      final int MAX_RESULT = 5;
      final int MAX_NAVIGATION_PAGE = 10;
      
      PaginationResult<OrderInfo> paginationResult  = orderDAO.listOrderInfo(page, MAX_RESULT, MAX_NAVIGATION_PAGE);
      model.addAttribute("paginationResult", paginationResult);
      
      List<OrderDetailInfo> details = this.orderDAO.listOrderDetailInfos(orderId);
      
      List<String>idOrderDetailList = new ArrayList<String>();
      String idOrderInfo = "";
      for(OrderDetailInfo orderDetailInfo : details){
    	  idOrderInfo = orderDetailInfo.getId();
    	  idOrderDetailList.add(idOrderInfo);
      }
      session.setAttribute("idOrderDetailList", idOrderDetailList);
      
      orderInfo.setDetails(details);
      model.addAttribute("orderInfo", orderInfo);
   
      return "orderDetailEdit";
   }
   
   
   // POST: Update quantity for product in cart
   @RequestMapping(value = { "/admin/orderDetailEdit" }, method = RequestMethod.POST)
   public String shoppingCartUpdateQty( HttpSession session, //
         Model model, //
         @ModelAttribute("orderInfo") OrderInfo orderInfo, @ModelAttribute("paginationResult") OrderInfo paginationResult, 
         MultipartHttpServletRequest request) {
 
	   String orderId = (String)session.getAttribute("orderId");
	   List<String> idOrderInfoList =  (List<String>) session.getAttribute("idOrderDetailList");
	   if(orderInfo != null && orderId != null) {
		   	this.orderDAO.updateQuantityAndSave(orderInfo, orderId, idOrderInfoList);
		   	session.removeAttribute("orderInfo");
		   	session.removeAttribute("idOrderDetailList");
	   }
 
      return "order";
   }
   
   // orders detail edit 
   @RequestMapping(value = { "/admin/orderDetailCustEdit" }, method = RequestMethod.GET)
   public String orderEdit(HttpServletRequest request, HttpSession session ,  Model model, @RequestParam("orderId") String orderId) {
      OrderInfo orderInfo = null;
      
      if (orderId != null) {
    	 session.setAttribute("orderId", orderId);
         orderInfo = this.orderDAO.getOrderInfo(orderId);
      }
      

      if (orderInfo == null) {
         return "redirect:/admin/orderList";
      }

      
      List<OrderDetailInfo> details = this.orderDAO.listOrderDetailInfos(orderId);
      orderInfo.setDetails(details);
      model.addAttribute("orderInfo", orderInfo);
   
      return "orderDetailCustEdit";
   }
  
 
   // POST: Save customer information.
   @RequestMapping(value = { "/admin/orderDetailCustEdit" }, method = RequestMethod.POST)
   public String editOrderSave(HttpServletRequest request, HttpSession session,//
         Model model, //
         @ModelAttribute("orderInfo") OrderInfo orderInfo,         
         //
         BindingResult result, //
         final RedirectAttributes redirectAttributes) {
 
	  
      String orderId = (String)session.getAttribute("orderId");
      if (result.hasErrors()) {
         // Forward to reenter customer info.
         return "redirect:/admin/orderDetailCustEdit?"+orderId;
      }
 
      try {
    	  orderDAO.saveCustommerOrder(orderInfo, orderId);
    	  session.removeAttribute("orderId");
       } catch (Exception e) {
          Throwable rootCause = ExceptionUtils.getRootCause(e);
          String message = rootCause.getMessage();
          model.addAttribute("errorMessage", message);
          // Show product form.
          return "orderList";
       }
      int page = 1;
      final int MAX_RESULT = 5;
      final int MAX_NAVIGATION_PAGE = 10;
 
      PaginationResult<OrderInfo> paginationResult //
            = orderDAO.listOrderInfo(page, MAX_RESULT, MAX_NAVIGATION_PAGE);
      model.addAttribute("paginationResult", paginationResult);
      return "orderList";
   }
   
   
   // Delete Account
   @RequestMapping(value = { "/admin/deleteAccount" }, method = RequestMethod.GET)
   public String deleteAccount(HttpServletRequest request, HttpServletResponse response, Model model,
         @RequestParam("username") String username) throws IOException {
      Account account = null;
      if (username != null) {
          account = this.accountDAO.findAccount(username);
          if(account != null) {
        	  this.accountDAO.deleteAccount(username);
          }
      }
      
      return "redirect:/admin/accountList";
      
   }
   
   // Delete Order
   @RequestMapping(value = { "/admin/deleteOrder" }, method = RequestMethod.GET)
   public String deleteOrder(Model model, @RequestParam("orderId") String orderId) {
	      Order order = null;
	      if (orderId != null) {
	    	   order = this.orderDAO.findOrder(orderId);
	    	   if(order != null) {
	               this.orderDAO.deleteOrder(orderId);
	    	   }
	      }
	     
      return "redirect:/admin/orderList";     
   }
 
}
