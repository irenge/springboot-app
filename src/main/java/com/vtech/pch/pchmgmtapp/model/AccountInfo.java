package com.vtech.pch.pchmgmtapp.model;

import java.util.Date;

public class AccountInfo {
	
	private String userName;
	private boolean active;
	private String encrytedPassword;
	private String userRole;
	private Date createAt;
	private Date loggedDate;
	
	
	
	
	//Using in JPA and queries
	public AccountInfo(String userName, boolean active, String encrytedPassword, String userRole, Date createAt,
			Date loggedDate) {
		super();
		this.userName = userName;
		this.active = active;
		this.encrytedPassword = encrytedPassword;
		this.userRole = userRole;
		this.createAt = createAt;
		this.loggedDate = loggedDate;
	}

	public String getUsername() {
		return userName;
	}
	public void setUsername(String username) {
		this.userName = username;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getPassword() {
		return encrytedPassword;
	}
	public void setPassword(String password) {
		this.encrytedPassword = password;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public Date getLoggedDate() {
		return loggedDate;
	}
	public void setLoggedDate(Date loggedDate) {
		this.loggedDate = loggedDate;
	}
	
	
 
}
