package com.vtech.pch.pchmgmtapp.model;

import com.vtech.pch.pchmgmtapp.entity.Product;

public class ProductInfo {
    private String code;
    private String name;
    private double price;
    private String details;
 
    public ProductInfo() {
    }
 
    public ProductInfo(Product product) {
        this.code = product.getCode();
        this.name = product.getName();
        this.price = product.getPrice();
        this.details= product.getDetails();
    }
 
    // Using in JPA/Hibernate query
    public ProductInfo(String code, String name, double price, String details) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.details = details;
    }
       
 
    public String getCode() {
        return code;
    }
 
    public void setCode(String code) {
        this.code = code;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public double getPrice() {
        return price;
    }
 
    public void setPrice(double price) {
        this.price = price;
    }

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
}
