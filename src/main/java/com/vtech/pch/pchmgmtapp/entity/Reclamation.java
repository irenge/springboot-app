package com.vtech.pch.pchmgmtapp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "Claim_form")
public class Reclamation implements Serializable{
	
	private static final long serialVersionUID = -1000119078147252957L;
	
    @Id
    @Column(name = "id", length = 20, nullable = false)
    private String id;
 
    @Column(name = "Name", length = 100, nullable = false)
    private String name;
    
    @NotEmpty(message = "You haven't enter a mail address!")
    @Column(name = "Email", length = 100, nullable = false)
    private String email;

    @Column(name = "Subject", length = 200, nullable = false)
    private String subject;

    @NotEmpty(message = "The message can not be empty!")
    @Column(name = "Message", length = 800, nullable = false)
    private String message;

    public Reclamation(){}
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

    

}
