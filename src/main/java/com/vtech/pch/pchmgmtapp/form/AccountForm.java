package com.vtech.pch.pchmgmtapp.form;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.vtech.pch.pchmgmtapp.entity.Account;

public class AccountForm {

    private String username;
	private boolean active;
	private String password;
	private String userRole;
	private Date createdAt;
	private Date loggedDate;
	
	// Upload file.
    private MultipartFile fileData;
    
    private boolean newAccount = false;
	
    public AccountForm(){
		this.setNewAccount(true);
	}
    
    public AccountForm(Account account){
    	this.setUsername(account.getUserName());
    	this.active =   account.isActive();
    	this.password = account.getEncrytedPassword();
    	this.userRole = account.getUserRole();
    	this.createdAt = account.getCreateAt();
    	this.loggedDate = account.getLoggedDate();
    }
	
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	

	public MultipartFile getFileData() {
		return fileData;
	}

	public void setFileData(MultipartFile fileData) {
		this.fileData = fileData;
	}

	public boolean isNewAccount() {
		return newAccount;
	}

	public void setNewAccount(boolean newAccount) {
		this.newAccount = newAccount;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getLoggedDate() {
		return loggedDate;
	}

	public void setLoggedDate(Date loggedDate) {
		this.loggedDate = loggedDate;
	}
  
}
