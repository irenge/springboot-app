package com.vtech.pch.pchmgmtapp.form;

public class SearchForm {

	private String inputVal;

	public SearchForm(){}
	
	public String getInputVal() {
		return inputVal;
	}

	public void setInputVal(String inputVal) {
		this.inputVal = inputVal;
	}
	
}
